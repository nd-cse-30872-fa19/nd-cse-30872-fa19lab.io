title:      "Challenge 08: The Knight's Dialer"
icon:       fa-code
navigation: []
internal:
external:
body:       |

    This summer **Caleb** went to a day camp where he learned how to play
    **chess**.  He quickly learned the basic rules of chess and grew to love
    playing the game (almost as much as playing **Minecraft**).  On some
    evenings, he will challenge the instructor to a game after dinner (and
    sometimes even win!). 
    
    <div class="thumbnail">
    <img src="static/img/challenge08-chess.jpg" class="img-response">
    </div>
    
    In chess each piece can move in a different manner.  For instance,
    the **rook**, Caleb's favorite piece, can move any number of spaces vertically
    or horizontally.  A **knight**, however, can only move in a `L`-shape motion:
    two steps horizontally followed by one vertically, or one step horizontally
    then two vertically:

    Unfortunately, Caleb really dislikes the **knight** piece because its
    movement is so confusing.  To help Caleb practice visualize the movement of
    a knight, the instructor proposes the following problem to his son:

    > Imagine you place a knight chess piece on a phone dialpad.  Suppose you
    dial keys on the keypad using only hops a knight can make. Every time the
    knight lands on a key, we dial that key and make another hop. The starting
    position counts as being dialed.

    <div class="thumbnail">
    <img src="https://hackernoon.com/hn-images/1*pE4b3hqGDv7pKivQTQZyPw.png"
    class="img-responsive">
    </div>

    > What are the **distinct numbers** you can dial in `N` hops from a
    particular starting position?

    Because solving this problem efficient requires the use of **recursion**,
    Caleb asks that you help him out by programming a solution that solves this
    challenge.

    ## Input
    
    You will be given a series of starting positions and hops in the following format:

        :::text
        START HOPS
        ...
        START HOPS

    ### Example Input

        :::text
        6 3
        0 4

    ## Output

    For each pair of starting positions and hops, output all possible numbers
    starting with the specified position and of the specified hops formed on
    the dialpad by using only the chess knight's motion.

    ### Example Output

        :::text
        604
        606
        616
        618
        672
        676

        0404
        0406
        0434
        0438
        0492
        0494
        0604
        0606
        0616
        0618
        0672
        0676

    **Note**: Output the numbers in **ascending order** and ensure there is a
    single **blank line** between the outputs of the pairs of starting
    positions and hops.

    <div class="alert alert-info" markdown="1">
    #### <i class="fa fa-lightbulb-o"></i> Programming Challenges

    This is inspired by this blog post: [Google Interview Questions
    Deconstructed: The Knight’s Dialer](https://hackernoon.com/google-interview-questions-deconstructed-the-knights-dialer-f780d516f029)
    and this [935. Knight Dialer](https://leetcode.com/problems/knight-dialer/) on [LeetCode].

    </div>

    [LeetCode]: https://leetcode.com

    ## Submission

    To submit your work, follow the same procedure you used for [Reading 00]:

        :::bash
        $ cd path/to/cse-30872-fa19-assignments     # Go to assignments repository
        $ git checkout master                       # Make sure we are on master
        $ git pull --rebase                         # Pull any changes from GitLab

        $ git checkout -b challenge08               # Create and checkout challenge08 branch

        $ $EDITOR challenge08/program.cpp           # Edit your code

        $ git add challenge08/program.cpp           # Stage your changes
        $ git commit -m "challenge08: done"         # Commit your changes

        $ git push -u origin challenge08            # Send changes to GitLab

    To check your code, you can use the `.scripts/submit.py` script or [curl]:

        $ .scripts/submit.py
        Submitting challenge08 assignment ...

        Submitting challenge08 code ...
          Result Success
           Score 6.00
            Time 0.03

        $ curl -F source=@challenge08/program.cpp  https://dredd.h4x0r.space/code/cse-30872-fa19/challenge08
        {"score": 6, "result": "Success"}

    Once you have commited your work and pushed it to [GitLab], member to
    [create a merge request].  Refer to the [Reading 04 TA List] to determine
    your corresponding TA for the merge request.

    [Reading 00]:               reading00.html
    [dredd]:                    https://dredd.h4x0r.space
    [Reading 04 TA List]:       reading04_tas.html
    [create a merge request]:   https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [GitLab]:                   https://gitlab.com
    [curl]:                     https://curl.haxx.se/
