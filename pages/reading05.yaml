title:      "Reading 05: Greedy Algorithms"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will work on problems related to [greedy algorithms], which
    is a problem solving approach where we attempt to find a global optimum by
    always making the locally optimal choice, and [bit manipulation].  These
    techniques will come in handy for solving [Challenge 09] and [Challenge
    10].

    [Challenge 09]: challenge09.html
    [Challenge 10]: challenge10.html

    [Bit Manipulation]:     https://en.wikipedia.org/wiki/Bit_manipulation
    [Greedy Algorithms]:    https://en.wikipedia.org/wiki/Greedy_algorithm

    ## Reading

    The readings for this week are:

    1. [Competitive Programmer's Handbook]

        - Chapter 6 Greedy Algorithm

        - Chapter 10 Bit manipulation

    [C++]:      https://isocpp.org/
    [Python]:   https://www.python.org/

    ## Quiz

    Once you have done the readings, answer the following [Reading 05 Quiz]
    questions:

    <div id="quiz-questions"></div>

    <div id="quiz-responses"></div>

    <script src="static/js/dredd-quiz.js"></script>
    <script>
    loadQuiz('static/json/reading05.json');
    </script>

    ## Submission

    To submit you work, follow the same process outlined in [Reading 00]:

        :::bash
        $ git checkout master                 # Make sure we are in master branch
        $ git pull --rebase                   # Make sure we are up-to-date with GitLab

        $ git checkout -b reading05           # Create reading05 branch and check it out

        $ cd reading05                        # Go into reading05 folder
        $ $EDITOR answers.json                # Edit your answers.json file

        $ ../.scripts/submit.py               # Check reading05 quiz
        Submitting reading05 assignment ...
        Submitting reading05 quiz ...
              Q1 0.25
              Q2 0.25
              Q3 0.25
              Q4 0.25
              Q5 0.20
              Q6 0.50
              Q7 0.25
           Score 2.00

        $ git add answers.json                # Add answers.json to staging area
        $ git commit -m "Reading 05: Done"    # Commit work

        $ git push -u origin reading05        # Push branch to GitLab

    Remember to [create a merge request] and assign the appropriate TA from the
    [Reading 05 TA List].

    [GitLab]:                               https://gitlab.com
    [Reading 00]:                           reading00.html
    [Reading 05 Quiz]:                      static/json/reading05.json
    [JSON]:                                 http://www.json.org/
    [git-branch]:                           https://git-scm.com/docs/git-branch
    [dredd]:                                https://dredd.h4x0r.space
    [create a merge request]:               https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [Reading 05 TA List]:                   reading05_tas.html
    [Competitive Programmer's Handbook]:    https://cses.fi/book.html
