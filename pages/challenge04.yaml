title:      "Challenge 04: Baraka Bouts"
icon:       fa-code
navigation: []
internal:
external:
body:       |

    <img src="static/img/challenge04-baraka.jpg" class="pull-right" style="padding-left: 10px">

    Congratulations to our hero protagonist, [Cami Carballo], for joining
    [Baraka Bouts], which if you didn't know is an on campus organization that:

    > Founded in 1997, the Women's Boxing Club known as Baraka Bouts is the
    largest all-female club at the University of Notre Dame. From September to
    November, hundreds of women are given the opportunity to participate in the
    most unique Notre Dame experience. Pairing service with athletic ability,
    Baraka Bouts offers an experience no student can forget.

    Each year, the club takes a group photo of all the women boxers.  Because
    the club members are divided into "blue" and "gold" teams[^1], the photo
    usually involves the "blue" team standing behind the "gold" team.  Of
    course, since this photo is suppose to commemorate the year's club members,
    it would be best if each member in the "blue" team was taller than the
    "gold" team member in front of them; This way, everyone would be visible
    in the photo.

    Given the heights of all the members in the "blue" and "gold" teams,
    determine if there is a possible arrangement such that each "blue" team
    member is taller than the "gold" team member in front of them.

    ## Input

    You will be given a series of "gold" and "blue" team member heights:

    ```
    gold1 gold2 gold3...
    blue1 blue2 blue3...
    gold1 gold2 gold3...
    blue1 blue2 blue3...
    ```

    That is, for each pair of lines, the first line corresponds to all the
    heights of the "gold" team members, and the second line corresponds to all
    the heights of the "blue" team members.

    Here is an example input:

    ```
    1 2 3 4
    2 3 4 5
    1 5 4
    2 3 4
    ```

    ## Output

    For each pair of "gold" and "blue" members, you are determine if there is
    any possible arrangement such that each "blue" team member is taller than
    the "gold" team member in front of them.  If so, output "Yes"; otherwise,
    output "No".

    Here is the output for the example input above:

        :::text
        Yes
        No

    ## Submission

    To submit your work, follow the same procedure you used for [Reading 00]:

        :::bash
        $ cd path/to/cse-30872-fa19-assignments     # Go to assignments repository
        $ git checkout master                       # Make sure we are on master
        $ git pull --rebase                         # Pull any changes from GitLab

        $ git checkout -b challenge04               # Create and checkout challenge04 branch

        $ $EDITOR challenge04/program.cpp           # Edit your code

        $ git add challenge04/program.cpp           # Stage your changes
        $ git commit -m "challenge04: done"         # Commit your changes

        $ git push -u origin challenge04            # Send changes to GitLab

    To check your code, you can use the `.scripts/submit.py` script or [curl]:

        $ .scripts/submit.py
        Submitting challenge04 assignment ...
        Submitting challenge04 code ...
          Result Success
           Score 6.00

        $ curl -F source=@challenge04/program.cpp https://dredd.h4x0r.space/code/cse-30872-fa19/challenge04
        {"score": 6, "result": "Success"}

    Once you have commited your work and pushed it to [GitLab], member to
    [create a merge request].  Refer to the [Reading 02 TA List] to determine
    your corresponding TA for the merge request.

    [Reading 00]:               reading00.html
    [Reading 02 TA List]:       reading02_tas.html
    [dredd]:                    https://dredd.h4x0r.space
    [create a merge request]:   https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
    [GitLab]:                   https://gitlab.com
    [curl]:                     https://curl.haxx.se/

    [Cami Carballo]:            http://camigcarballo.com/
    [Baraka Bouts]:             https://www3.nd.edu/~wboxing/
    [Computer Science and Engineering]: https://cse.nd.edu

    [^1]: I just made this up.  They wear blue and gold trunks, but they aren't in teams.
